//
//  WLViewController.m
//  WeatherLayer
//
//  Created by Alex on 11/8/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import "WLViewController.h"
#import "WLTestView.h"

@interface WLViewController ()


@end

@implementation WLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor blackColor]];
    WLTestView *test = [[WLTestView alloc] initWithFrame:CGRectMake(0, 64, 320, 456)];
    [self.view addSubview:test];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
