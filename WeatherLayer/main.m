//
//  main.m
//  WeatherLayer
//
//  Created by Alex on 11/8/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WLAppDelegate class]));
    }
}
