//
//  WLView.h
//  WeatherLayer
//
//  Created by Alex on 11/26/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kSnowyLight,
    kSnowyMedium,
    kSnowyHard,
    kSnowyCustom,
    kCloudlyRainLight,
    kCloudlyRainMedium,
    kCloudlyRainHard,
    kCloudlyRainCustom,
    kPartly,
    kCloudly,
    kClear,
    kSunny
} WeatherType;

@interface WLView : UIView

- (void)setWeather:(WeatherType)type withDefaultThemeWithAnimation:(BOOL)isAnimated;
- (void)setWeather:(WeatherType)type withXMASTheme:(BOOL)isXMAS withAnimation:(BOOL)isAnimated;
- (void)setWeather:(WeatherType)type withSochiTheme:(BOOL)isSochi withAnimation:(BOOL)isAnimated;
- (void)setSnowCustomVelocity:(float)velocity;
- (void)setSnowCustomBirthrate:(float)birthrate;
- (void)setRainCustomVelocity:(float)velocity;
- (void)setRainCustomBirthrate:(float)birthrate;

- (void)resumeAnimation;
- (void)setXMASTheme:(BOOL)isXMAS;
- (void)setSochiTheme:(BOOL)isSochi;

@end
