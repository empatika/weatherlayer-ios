//
//  WLView.m
//  WeatherLayer
//
//  Created by Alex on 11/26/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import "WLView.h"
#import <QuartzCore/QuartzCore.h>
#import "WLConstraint.h"

struct Pair
{
    float first;
    float second;
};

@interface WLView ()
{
    CALayer *_backgroundLayer;
    CAEmitterLayer *_emitterLayer;
    CAShapeLayer *_cloudSublayer;
    CAShapeLayer *_cloudSublayer2;
    CALayer *_sunLayer;
    CALayer *_xmasLayer;
    CALayer *_sochiLayer;
    
    WeatherType _currentWeather;
    BOOL _isAnimated;
    BOOL _xmasTheme;
    BOOL _sochiTheme;
    BOOL _isSochi;
}

@end

@implementation WLView

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resumeAnimation)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:[UIApplication sharedApplication]];
        [self initLayers];
        if (![[NSUserDefaults standardUserDefaults] floatForKey:@"RAIN_CUSTOM_BIRTHRATE"])
        {
            [[NSUserDefaults standardUserDefaults] setFloat:600.f forKey:@"RAIN_CUSTOM_BIRTHRATE"];
        }
        if (![[NSUserDefaults standardUserDefaults] floatForKey:@"SNOW_CUSTOM_BIRTHRATE"])
        {
            [[NSUserDefaults standardUserDefaults] setFloat:300.f forKey:@"SNOW_CUSTOM_BIRTHRATE"];
        }
        if (![[NSUserDefaults standardUserDefaults] floatForKey:@"RAIN_CUSTOM_VELOCITY"])
        {
            [[NSUserDefaults standardUserDefaults] setFloat:300.f forKey:@"RAIN_CUSTOM_VELOCITY"];
        }
        if (![[NSUserDefaults standardUserDefaults] floatForKey:@"SNOW_CUSTOM_VELOCITY"])
        {
            [[NSUserDefaults standardUserDefaults] setFloat:40.f forKey:@"SNOW_CUSTOM_VELOCITY"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return self;
}

- (void)resumeAnimation
{
    [self setWeather:_currentWeather withAnimation:_isAnimated];
}

- (void)setXMASTheme:(BOOL)isXMAS
{
    _xmasTheme = isXMAS;
    if (isXMAS) {
        _sochiTheme = NO;
    }
    [self resumeAnimation];
}

- (void)setSochiTheme:(BOOL)isSochi
{
    _sochiTheme = isSochi;
    if (isSochi) {
        _xmasTheme = NO;
    }
    [self resumeAnimation];
}

- (void)setRainCustomBirthrate:(float)birthrate
{
    [[NSUserDefaults standardUserDefaults] setFloat:birthrate forKey:@"RAIN_CUSTOM_BIRTHRATE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setRainCustomVelocity:(float)velocity
{
    [[NSUserDefaults standardUserDefaults] setFloat:velocity forKey:@"RAIN_CUSTOM_VELOCITY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setSnowCustomBirthrate:(float)birthrate
{
    [[NSUserDefaults standardUserDefaults] setFloat:birthrate forKey:@"SNOW_CUSTOM_BIRTHRATE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setSnowCustomVelocity:(float)velocity
{
    [[NSUserDefaults standardUserDefaults] setFloat:velocity forKey:@"SNOW_CUSTOM_VELOCITY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setWeather:(WeatherType)type withXMASTheme:(BOOL)isXMAS withAnimation:(BOOL)isAnimated
{
    _xmasTheme = YES;
    _sochiTheme = NO;
    [self setWeather:type withAnimation:isAnimated];
}

- (void)setWeather:(WeatherType)type withSochiTheme:(BOOL)isSochi withAnimation:(BOOL)isAnimated
{
    _xmasTheme = NO;
    _sochiTheme = YES;
    [self setWeather:type withAnimation:isAnimated];
}

- (void)setWeather:(WeatherType)type withDefaultThemeWithAnimation:(BOOL)isAnimated
{
    _xmasTheme = NO;
    _sochiTheme = NO;
    [self setWeather:type withAnimation:isAnimated];
}

- (void)setWeather:(WeatherType)type withAnimation:(BOOL)isAnimated
{
    _currentWeather = type;
    _isAnimated = isAnimated;
    
    if (isAnimated)
    {
        [self initLayersWithWeatherType:type];
    }
    else
    {
        [self initLayers];
    }
    
    switch (type) {
        case kSunny:
            [self setSunnyWeather];
            break;
        case kCloudly:
            [self setCloudlyWeather];
            break;
        case kPartly:
            [self setPartlyWeather];
            break;
        case kClear:
            [self setClearWeather];
            break;
        case kCloudlyRainLight:
            [self setCloudlyRainWeather:kLightPrecipitation];
            break;
        case kCloudlyRainMedium:
            [self setCloudlyRainWeather:kMediumPrecipitation];
            break;
        case kCloudlyRainHard:
            [self setCloudlyRainWeather:kHardPrecipitation];
            break;
        case kCloudlyRainCustom:
            [self setCloudlyRainWeather:kCustomPrecipitation];
            break;
        case kSnowyLight:
            [self setSnowyWeather:kLightPrecipitation];
            break;
        case kSnowyMedium:
            [self setSnowyWeather:kMediumPrecipitation];
            break;
        case kSnowyHard:
            [self setSnowyWeather:kHardPrecipitation];
            break;
        case kSnowyCustom:
            [self setSnowyWeather:kCustomPrecipitation];
            break;
        default:
            break;
    }
    
    if (_xmasTheme)
    {
        _xmasLayer.frame = CGRectMake(0, self.bounds.size.height - 200, 320, 200);
        _xmasLayer.contents = (id)[UIImage imageNamed:@"xmas_bg"].CGImage;
    }
    else if (_sochiTheme)
    {
        _sochiLayer.frame = CGRectMake(0, self.bounds.size.height - 230, 320, 230);
        _sochiLayer.contents = (id)[UIImage imageNamed:@"sochi"].CGImage;
    }
}

#pragma mark – Snowy Weather

- (void)setSnowyWeather:(PrecipitationType)type
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"clear_background.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"clear_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"clear"].CGImage;
    [self setSnowyRaindrop:type];
    [self setWeather];
    [self setSnowyWeatherCloudAnimation];
}

- (void)setSnowyWeatherCloudAnimation
{
    struct Pair yComponents = {95.f, 55.f};
    struct Pair durations = {550.f, 430.f};
    struct Pair timeOffsets = {185.f, 170.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];
}

- (void)setSnowyRaindrop:(PrecipitationType)type
{
    CAEmitterCell *cell = [CAEmitterCell emitterCell];
    [cell setName:@"snowflakes"];
    switch (type) {
        case kCustomPrecipitation:
            [cell setBirthRate:[[NSUserDefaults standardUserDefaults] floatForKey:@"SNOW_CUSTOM_BIRTHRATE"]];
            [cell setVelocity:[[NSUserDefaults standardUserDefaults] floatForKey:@"SNOW_CUSTOM_VELOCITY"]];
            break;
        case kHardPrecipitation:
            [cell setBirthRate:SNOW_HARD_BIRTHRATE];
            [cell setVelocity:SNOW_HARD_VELOCITY];
            break;
        case kMediumPrecipitation:
            [cell setBirthRate:SNOW_MEDIUM_BIRTHRATE];
            [cell setVelocity:SNOW_MEDIUM_VELOCITY];
            break;
        case kLightPrecipitation:
            [cell setBirthRate:SNOW_LIGHT_BIRTHRATE];
            [cell setVelocity:SNOW_LIGHT_VELOCITY];
            break;
        default:
            break;
    }
    [cell setVelocityRange:20];
    [cell setYAcceleration:1.0f];
    [cell setEmissionLongitude:M_PI];
    [cell setEmissionLongitude:M_PI];
    [cell setEmissionRange:0.4];
    [cell setScale:.2f];
    [cell setContents:(id)[UIImage imageNamed:@"snow_particle.png"].CGImage];
    [cell setLifetime:8.0f];
    [cell setLifetimeRange:2.0f];
    [cell setSpin:3.f];
    [cell setMagnificationFilter:kCAFilterTrilinear];
    [cell setMinificationFilter:kCAFilterLinear];
    [self setEmitterLayerWithCell:cell];
}

#pragma mark – Cloudly Rain Weather

- (void)setCloudlyRainWeather:(PrecipitationType)type
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"cloudly_background.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"cloudly_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"cloudly_cloud.png"].CGImage;
    [self setCloudlyRaindrop:type];
    [self setWeather];
    [self setCloudlyRainWeatherCloudAnimation];
}

- (void)setCloudlyRainWeatherCloudAnimation
{
    struct Pair yComponents = {115.f, 85.f};
    struct Pair durations = {550.f, 430.f};
    struct Pair timeOffsets = {225.f, 210.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];
}

- (void)setCloudlyRaindrop:(PrecipitationType)type
{
    CAEmitterCell *cell = [CAEmitterCell emitterCell];
    [cell setName:@"raindrop"];
    switch (type) {
        case kCustomPrecipitation:
            [cell setBirthRate:[[NSUserDefaults standardUserDefaults] floatForKey:@"RAIN_CUSTOM_BIRTHRATE"]];
            [cell setVelocity:[[NSUserDefaults standardUserDefaults] floatForKey:@"RAIN_CUSTOM_VELOCITY"]];
            break;
        case kHardPrecipitation:
            [cell setBirthRate:RAIN_HARD_BIRTHRATE];
            [cell setVelocity:RAIN_HARD_VELOCITY];
            break;
        case kMediumPrecipitation:
            [cell setBirthRate:RAIN_MEDIUM_BIRTHRATE];
            [cell setVelocity:RAIN_MEDIUM_VELOCITY];
            break;
        case kLightPrecipitation:
            [cell setBirthRate:RAIN_LIGHT_BIRTHRATE];
            [cell setVelocity:RAIN_LIGHT_VELOCITY];
            break;
        default:
            break;
    }
    [cell setVelocity:300];
    [cell setVelocityRange:70];
    [cell setYAcceleration:8.0f];
    [cell setEmissionLongitude:M_PI];
    [cell setEmissionRange:0.];
    [cell setScale:.3f];
    [cell setContents:(id)[UIImage imageNamed:@"rain_particle.png"].CGImage];
    [cell setLifetime:8.0f];
    [cell setLifetimeRange:2.0f];
    [self setEmitterLayerWithCell:cell];
}

#pragma mark – Partly Weather

- (void)setPartlyWeather
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"partly_background.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"partly_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"partly_cloud.png"].CGImage;
    [self setWeather];
    [self setPartlyWeatherCloudAnimation];
}

- (void)setPartlyWeatherCloudAnimation
{
    struct Pair yComponents = {55.f, 20.f};
    struct Pair durations = {550.f, 430.f};
    struct Pair timeOffsets = {195.f, 180.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];}

#pragma mark – Cloudly Weather

- (void)setCloudlyWeather
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"cloudly_background.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"cloudly_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"cloudly_cloud.png"].CGImage;
    [self setWeather];
    [self setCloudlyWeatherCloudAnimation];
}

- (void)setCloudlyWeatherCloudAnimation
{
    struct Pair yComponents = {115.f, 75.f};
    struct Pair durations = {550.f, 430.f};
    struct Pair timeOffsets = {235.f, 210.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];
}

#pragma mark – Clear Weather

- (void)setClearWeather
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"clear_background.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"clear_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(0, 0, 801, 486);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"clear_cloud.png"].CGImage;
    [self setWeather];
    [self setClearWeatherCloudAnimation];
}

- (void)setClearWeatherCloudAnimation
{
    struct Pair yComponents = {35.f, 40.f};
    struct Pair durations = {550.f, 430.f};
    struct Pair timeOffsets = {225.f, 140.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];
}

#pragma mark – Sunny Weather

- (void)setSunnyWeather
{
    _backgroundLayer.frame = self.bounds;
    _backgroundLayer.contents = (id)[UIImage imageNamed:@"sunny_background.png"].CGImage;
    _sunLayer.frame = CGRectMake(_backgroundLayer.frame.size.width - 299, -130, 299, 490);
    _sunLayer.contents = (id)[UIImage imageNamed:@"sun_corner.png"].CGImage;
    _cloudSublayer.frame = CGRectMake(0, 0, 398, 207);
    _cloudSublayer.contents = (id)[UIImage imageNamed:@"sunny_cloud.png"].CGImage;
    _cloudSublayer2.frame = CGRectMake(-290, 0, 398, 207);
    _cloudSublayer2.contents = (id)[UIImage imageNamed:@"sunny_cloud.png"].CGImage;
    [self setWeather];
    [self setSunnyWeatherCloudAnimation];
}

- (void)setSunnyWeatherCloudAnimation
{
    struct Pair yComponents = {75.f, 80.f};
    struct Pair durations = {250.f, 190.f};
    struct Pair timeOffsets = {125.f, 40.f};
    [self setCloudAnimationWithYComponentsOfClouds:yComponents withDurations:durations withTimeOffsets:timeOffsets];
}

#pragma mark – Common Methods

- (void)initLayers
{
    _backgroundLayer = [CALayer layer];
    _cloudSublayer = [CAShapeLayer layer];
    _cloudSublayer2 = [CAShapeLayer layer];
    _xmasLayer = [CALayer layer];
    _sunLayer = [CALayer layer];
    _emitterLayer = [CAEmitterLayer layer];
}

- (void)initLayersWithWeatherType:(WeatherType)type
{
    if (type == kPartly || type == kCloudly
        || type == kClear || type == kSunny)
    {
        [_emitterLayer removeFromSuperlayer];
        _emitterLayer = [CAEmitterLayer layer];
    }
    [_sunLayer removeFromSuperlayer];
    _sunLayer = [CALayer layer];
    if (!_xmasTheme)
    {
        [_xmasLayer removeFromSuperlayer];
        _xmasLayer = [CALayer layer];
    }
    if (!_sochiTheme) {
        [_sochiLayer removeFromSuperlayer];
        _sochiLayer = [CALayer layer];
    }
}

- (void)setEmitterLayerWithCell:(CAEmitterCell *)cell
{
    [_emitterLayer setEmitterCells:@[cell]];
    [_emitterLayer setFrame:[_backgroundLayer frame]];
    CGPoint emitterPosition = (CGPoint) {[_backgroundLayer bounds].size.width / 2.f, 0.f};
    [_emitterLayer setEmitterPosition:emitterPosition];
    [_emitterLayer setEmitterSize:(CGSize){[_backgroundLayer bounds].size.width, 0.f}];
    [_emitterLayer setEmitterShape:kCAEmitterLayerLine];
    [_emitterLayer setRenderMode:kCAEmitterLayerUnordered];}

- (void)setWeather
{
    [_emitterLayer addSublayer:_cloudSublayer];
    [_emitterLayer addSublayer:_cloudSublayer2];
    [_backgroundLayer addSublayer:_emitterLayer];
    [_backgroundLayer addSublayer:_xmasLayer];
    [_backgroundLayer addSublayer:_sochiLayer];
    if (_sunLayer)
    {
        [_backgroundLayer addSublayer:_sunLayer];
    }
    [self.layer addSublayer:_backgroundLayer];
}

- (void)setCloudAnimationWithYComponentsOfClouds:(struct Pair)yComponents
                                   withDurations:(struct Pair)durations
                                 withTimeOffsets:(struct Pair)timeOffsets
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.repeatCount = 1000000000.f;
    [animation setFromValue:[NSValue valueWithCGPoint:CGPointMake(480.0f, yComponents.first)]];
    [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(-480.0f, yComponents.first)]];
    [animation setDuration:durations.first];
    [animation setTimeOffset:timeOffsets.first];
    CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"position"];
    animation2.repeatCount = 1000000000.f;
    [animation2 setFromValue:[NSValue valueWithCGPoint:CGPointMake(480.0f, yComponents.second)]];
    [animation2 setToValue:[NSValue valueWithCGPoint:CGPointMake(-480.0f, yComponents.second)]];
    [animation2 setDuration:durations.second];
    [animation2 setTimeOffset:timeOffsets.second];
    [_cloudSublayer addAnimation:animation forKey:@"position"];
    [_cloudSublayer2 addAnimation:animation2 forKey:@"position"];
}

@end
