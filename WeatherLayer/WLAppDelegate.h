//
//  WLAppDelegate.h
//  WeatherLayer
//
//  Created by Alex on 11/8/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
