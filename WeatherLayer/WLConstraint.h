//
//  WLConstraint.h
//  WeatherLayer
//
//  Created by Alex on 11/27/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#ifndef WeatherLayer_WLConstraint_h
#define WeatherLayer_WLConstraint_h

#define RAIN_LIGHT_BIRTHRATE 100.f
#define RAIN_MEDIUM_BIRTHRATE 400.f
#define RAIN_HARD_BIRTHRATE 800.f
#define RAIN_LIGHT_VELOCITY 200.f
#define RAIN_MEDIUM_VELOCITY 300.f
#define RAIN_HARD_VELOCITY 400.f

#define SNOW_LIGHT_BIRTHRATE 100.f
#define SNOW_MEDIUM_BIRTHRATE 200.f
#define SNOW_HARD_BIRTHRATE 400.f
#define SNOW_LIGHT_VELOCITY 20.f
#define SNOW_MEDIUM_VELOCITY 40.f
#define SNOW_HARD_VELOCITY 80.f

typedef enum {
    kLightPrecipitation,
    kMediumPrecipitation,
    kHardPrecipitation,
    kCustomPrecipitation
} PrecipitationType;

#endif
