//
//  WLTestView.m
//  WeatherLayer
//
//  Created by Alex on 11/27/13.
//  Copyright (c) 2013 Empatika. All rights reserved.
//

#import "WLTestView.h"

@interface WLTestView()
{
    int temp;
}

@end

@implementation WLTestView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        temp = 0;
        [self setWeather:0 withXMASTheme:YES withAnimation:YES];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    temp++;
    temp = temp % 36;
    int theme = temp / 3;
    if (temp % 3 == 0) {
        [self setWeather:theme withDefaultThemeWithAnimation:YES];
    } else if (temp % 3 == 1) {
        [self setWeather:theme withSochiTheme:YES withAnimation:YES];
    } else {
        [self setWeather:theme withXMASTheme:YES withAnimation:YES];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
